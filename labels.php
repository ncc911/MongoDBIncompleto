<?php
// This script is included in the showXYZ.php scripts.
// In those scripts $row has been initialized with the current post

echo '<h2>Etiquetas</h2>';	

// Show the tags of the current post

for($i = 0; $i<sizeof($row['tags']); $i++){
	echo "<a href='index.php?command=showPostsByTag&tag=" . $row['tags'][$i] . "'>" . $row['tags'][$i] . '</a>' . ' ';
}

/**********
** YOUR CODE HERE:
Iterate through the post (use the variable $row) and get the tags
For each tag, print an hyperlink with the tag text
The link points to index.php with two parameters:
* command =  showPostsByTag
* tag = the tag
**********/

?>
