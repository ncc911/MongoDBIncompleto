<?php
// Setup MongoDB connection
$connection = new MongoClient();

/**********
** YOUR CODE HERE:
Assign to $connection the connection to MongoDB
**********/

// Select the "posts" collection in the database "blog"

$collection = $connection->blog->posts;

/**********
** YOUR CODE HERE:
Assign to $collection the "posts" collection of the database "blog"
**********/

?>
