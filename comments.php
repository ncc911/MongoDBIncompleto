<?php
// This script is included in the showXYZ.php scripts.
// In those scripts $row has been initialized with the current post

echo '<h2>Comentarios</h2>';

// Show the comments of the current post
echo '<ul>';

for($i = 0; $i<sizeof($row['comments']); $i++){
	echo '<li>' . $row['comments'][$i]['author'] . ': ' . substr($row['comments'][$i]['body'], 0, 100) . ' ... </li>';
}
/**********
** YOUR CODE HERE:
Iterate through the post (use the variable $row) and get the comments
For each comment print only the first 100 characters (use the PHP substr() function)
**********/

echo '</ul>';
?>
